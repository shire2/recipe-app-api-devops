#!/bin/bash
# Tells linux its a bash script
sudo yum update -y #Update our yum package pamanger with latest packages
sudo amazon-linux extras install -y docker # Used to install docker
sudo systemctl enable docker.service # Enable docker, systemctl to keep it going after 
sudo systemctl start docker.service # Start docker, systemctl same
sudo usermod -aG docker ec2-user # Add EC2 user to add to the docker group
# Since using amazon standard ami image, amazon linux 2. using ec2, 
# default user is called ec2-user, which we log in as in bastion
# add to docker group which will allow to run and manager docker
# through the account